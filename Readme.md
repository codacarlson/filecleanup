FileCleanup
========================
Contributors: Joel Carlson(joel@codadevelopers.com), Zach Marcum(zmunky84@gmail.com)
Version: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This program is a simple program developed to poll a list of directories and automatically remove files given specific filters.

Description
========================

This program was developed to poll a set of directories and automatically remove files given specific filters. These filters include a prefix(accepts regex), a suffix, a minimum amount of seconds before a file can be deleted, and a disk usage threshold. There are options for removing files recursively as well as folders. This program accepts one parameter specifiying a configuration file. If a configuration file is given, all filters in the configuration file will be loaded at startup. The configuration file will also automatically save when filters are added, removed, or changed. 

Installation
========================
This program can be imported into eclipse by selecting "File/Import/Existing Projects into Workspace" and navigating to the downloaded folder location. The only runnable application is FileCleanup. This can also be built into an executable jar by selecting "File/Export/Runnable Jar". After being exported to a jar the jar can be ran using the following command.
	java -jar FileCleanup.jar

Changelog
========================
1.0 Initial Commit
