package application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/***************************************************************************
 * ListPanel *
 **************************************************************************/

public class TablePanel extends JPanel {

	/***************************************************************************
	 * Class Variables *
	 **************************************************************************/

	private static final long serialVersionUID = 4431177686687580509L;
	private TablePanelModel tableModel;
	private JPopupMenu popup;
	private JTable table;
	private TableListener tableListener;

	/***************************************************************************
	 * Constructors *
	 **************************************************************************/

	public TablePanel() {

		// Create and Show GUI
		createAndShowGUI();

	}

	/***************************************************************************
	 * Getters and Setters *
	 **************************************************************************/

	public TablePanelModel getTableModel() {
		return tableModel;
	}

	/***************************************************************************
	 * Methods *
	 **************************************************************************/

	public void addRow(FILTER filter) {
		tableModel.addRow(filter);
		refresh();
	}

	private void createAndShowGUI() {

		// Initialize Table Model
		tableModel = new TablePanelModel();

		// Initialize Table
		table = new JTable(tableModel);
		table.setRowHeight(25);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Add Table Listeners
		popup = new JPopupMenu();
		JMenuItem removeItem = new JMenuItem("Delete row");
		removeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = table.getSelectedRow();
				tableModel.removeRow(row);
				tableModel.fireTableRowsDeleted(row, row);
				if (tableListener != null)
					tableListener.tableChanged(tableModel.getFilterList());
			}
		});
		popup.add(removeItem);
		table.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {

				// Get Row
				int row = table.rowAtPoint(e.getPoint());
				table.getSelectionModel().setSelectionInterval(row, row);

				// Show Popup
				if (e.getButton() == MouseEvent.BUTTON3) {
					popup.show(table, e.getX(), e.getY());
				}
			}
		});

		// Layout GUI
		layoutGUI();
	}

	private void layoutGUI() {

		// Set Layout
		setLayout(new BorderLayout());

		// Add Table to Panel
		add(new JScrollPane(table), BorderLayout.CENTER);

		// Set Panel Properties
		setSize(800, 200);
		setMinimumSize(new Dimension(800, 100));
	}

	public void refresh() {
		tableModel.fireTableDataChanged();
	}

	public void setTableListener(TableListener tableListener) {
		this.tableListener = tableListener;
		this.tableModel.setTableListener(tableListener);
	}
}
