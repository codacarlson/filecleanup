package application;

import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/***************************************************************************
 * ListPanel *
 **************************************************************************/

public class TablePanelModel extends AbstractTableModel {

	/***************************************************************************
	 * Class Variables *
	 **************************************************************************/

	private static final long serialVersionUID = 1795753585955626336L;
	private List<FILTER> filterList;
	private String[] columnNames = { "Path", "Prefix", "Suffix", "Min Minutes",
			"Min Percentage", "Recursive", "Remove Directory" };
	private TableListener tableListener;

	/***************************************************************************
	 * Constructors *
	 **************************************************************************/

	public TablePanelModel() {
		filterList = new LinkedList<FILTER>();
	}

	/***************************************************************************
	 * Getters and Setters *
	 **************************************************************************/

	public List<FILTER> getFilterList() {
		return filterList;
	}

	public void setTableListener(TableListener tableListener) {
		this.tableListener = tableListener;
	}

	/***************************************************************************
	 * Methods *
	 **************************************************************************/

	public void addRow(FILTER filter) {
		filterList.add(filter);
		filter.getTimer().schedule(filter.getTask(), 0, 60000);
	}

	public Class<?> getColumnClass(int columnIndex) {
		return FILTER.types[columnIndex];
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int column) {
		return columnNames[column];
	}

	public int getRowCount() {
		return filterList.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			FILTER filter = filterList.get(rowIndex);

			switch (columnIndex) {
			case 0:
				return filter.getPath();
			case 1:
				return filter.getPrefix();
			case 2:
				return filter.getSuffix();
			case 3:
				return filter.getMinMinutes();
			case 4:
				return filter.getMinPercent();
			case 5:
				return filter.isRecursive();
			case 6:
				return filter.isRemoveDir();
			}
		} catch (IndexOutOfBoundsException e) {

		}
		return null;
	}

	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			filterList.get(rowIndex).setPath((String) value);
			break;
		case 1:
			filterList.get(rowIndex).setPrefix((String) value);
			break;
		case 2:
			filterList.get(rowIndex).setSuffix((String) value);
			break;
		case 3:
			filterList.get(rowIndex).setMinMinutes((Integer) value);
			break;
		case 4:
			filterList.get(rowIndex).setMinPercent((Double) value);
			break;
		case 5:
			filterList.get(rowIndex).setRecursive((Boolean) value);
			break;
		case 6:
			filterList.get(rowIndex).setRemoveDir((Boolean) value);
			break;
		}
		fireTableCellUpdated(rowIndex, columnIndex);
		if (tableListener != null)
			tableListener.tableChanged(filterList);
	}

	public boolean isCellEditable(int row, int col) {
		return true;
	}

	public void removeRow(int row) {
		filterList.get(row).getTimer().cancel();
		filterList.remove(row);
	}

}
