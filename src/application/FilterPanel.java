package application;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.Border;

/***************************************************************************
 * ListFilterPanel *
 **************************************************************************/

public class FilterPanel extends JPanel {

	/***************************************************************************
	 * Class Variables *
	 **************************************************************************/

	private static final long serialVersionUID = -9195941587947581237L;
	private FilterListener filterListener;
	private JLabel pathLabel;
	private JLabel prefixLabel;
	private JLabel suffixLabel;
	private JLabel minMinutesLabel;
	private JLabel minPercentLabel;
	private JLabel recursiveLabel;
	private JLabel removeDirLabel;
	private JTextField pathTextField;
	private JTextField prefixTextField;
	private JTextField suffixTextField;
	private JSpinner minMinutesSpinner;
	private SpinnerNumberModel minMinutesSpinnerModel;
	private JSpinner minPercentSpinner;
	private SpinnerNumberModel minPercentSpinnerModel;
	private JCheckBox recursiveCheckBox;
	private JCheckBox removeDirCheckBox;
	private JButton addButton;

	/***************************************************************************
	 * Constructors *
	 **************************************************************************/

	public FilterPanel() {

		// Create and Show GUI
		createAndShowGUI();

	}

	/***************************************************************************
	 * Methods *
	 **************************************************************************/

	public void addFilter() {
		FILTER filter = new FILTER();
		filter.setPath(pathTextField.getText());
		filter.setPrefix(prefixTextField.getText());
		filter.setSuffix(suffixTextField.getText());
		filter.setMinMinutes((Integer) minMinutesSpinner.getValue());
		filter.setMinPercent((Integer) minPercentSpinner.getValue());
		filter.setRecursive(recursiveCheckBox.isSelected());
		filter.setRemoveDir(removeDirCheckBox.isSelected());

		if (filterListener != null)
			filterListener.addFilter(filter);
	}

	public void createAndShowGUI() {

		// Initialize Components
		pathLabel = new JLabel("Path:");
		pathTextField = new JTextField();
		pathTextField.setMinimumSize(new Dimension(150, 25));
		pathTextField.setPreferredSize(new Dimension(150, 25));
		prefixLabel = new JLabel("Prefix:");
		prefixTextField = new JTextField();
		suffixLabel = new JLabel("Suffix:");
		suffixTextField = new JTextField();
		minMinutesLabel = new JLabel("Min. Minutes:");
		minMinutesSpinnerModel = new SpinnerNumberModel(0, 0,
				Integer.MAX_VALUE, 1);
		minMinutesSpinner = new JSpinner(minMinutesSpinnerModel);
		minMinutesSpinner.setPreferredSize(new Dimension(150, 25));
		minPercentLabel = new JLabel("Min. Percentage:");
		minPercentSpinnerModel = new SpinnerNumberModel(100, 0, 100, 1);
		minPercentSpinner = new JSpinner(minPercentSpinnerModel);
		minPercentSpinner.setPreferredSize(new Dimension(150, 25));
		recursiveLabel = new JLabel("Recursive:");
		recursiveCheckBox = new JCheckBox();
		removeDirLabel = new JLabel("Remove Directories:");
		removeDirCheckBox = new JCheckBox();
		addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				addFilter();
			}
		});

		// Layout GUI
		layoutGUI();
	}

	public void layoutGUI() {
		// Set Layout
		Border innerBorder = BorderFactory.createTitledBorder("Filter");
		Border outerBorder = BorderFactory.createEmptyBorder(0, 5, 0, 5);
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		// Path
		gc.gridx = 0;
		gc.gridy = 0;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(pathLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(pathTextField, gc);

		// Prefix
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(prefixLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(prefixTextField, gc);

		// Suffix
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(suffixLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(suffixTextField, gc);

		// Minimum Seconds
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(minMinutesLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(minMinutesSpinner, gc);

		// Minimum Percentage
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(minPercentLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(minPercentSpinner, gc);

		// Recursive
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(recursiveLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(recursiveCheckBox, gc);

		// Remove Directories
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 1.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		add(removeDirLabel, gc);
		gc.gridx = 1;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 10.0;
		gc.weighty = 1.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		add(removeDirCheckBox, gc);

		// Add Button
		gc.gridx = 0;
		gc.gridy++;
		gc.gridheight = 1;
		gc.gridwidth = 2;
		gc.weightx = 1.0;
		gc.weighty = 10.0;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.SOUTH;
		add(addButton, gc);
	}

	public void setFilterListener(FilterListener filterListener) {
		this.filterListener = filterListener;
	}
}
