package application;

import java.util.List;

public interface TableListener {
	public void tableChanged(List<FILTER> filterList);
}
