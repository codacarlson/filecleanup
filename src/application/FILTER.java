package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.FileUtils;

import utils.CleanupUtils;

/***************************************************************************
 * FILELIST *
 **************************************************************************/

public class FILTER {

	/***************************************************************************
	 * Class Variables *
	 **************************************************************************/

	private String path;
	private String prefix;
	private String suffix;
	private int minMinutes;
	private double minPercent;
	private boolean recursive;
	private boolean removeDir;
	private Timer timer;
	private TimerTask task;
	public static Class<?>[] types = { String.class, String.class,
			String.class, Integer.class, Double.class, Boolean.class,
			Boolean.class };

	/***************************************************************************
	 * Constructors *
	 **************************************************************************/

	public FILTER() {
		timer = new Timer();
		task = new TimerTask() {
			public void run() {
				File folder = new File(path);
				double freeSpace = ((double) folder.getUsableSpace() / (double) folder
						.getTotalSpace()) * 100.0;
				if (freeSpace < minPercent) {

					// List Directory
					List<File> files = new ArrayList<File>(
							CleanupUtils.listFileTree(folder, recursive,
									removeDir));

					// Filter Files by Name
					Iterator<File> iter = files.iterator();
					while (iter.hasNext()) {
						File file = iter.next();

						// Check Minimum Time
						long diff = (System.currentTimeMillis() - file
								.lastModified()) / (60 * 1000);
						if (!(diff >= minMinutes)) {
							iter.remove();
							continue;
						}
						
						// Check if Directory
						if ((removeDir) && (file.isDirectory())) {
							continue;
						}

						// Check Suffix
						if (!file.getName().endsWith(suffix)) {
							iter.remove();
							continue;
						}

						// Check Prefix
						String prefixRegex = prefix.replace("*", ".*").concat(
								".*");
						if (!file.getName().matches(prefixRegex)) {
							iter.remove();
							continue;
						}

					}

					// Sort by Last Modified
					Collections.sort(files, new Comparator<File>() {
						public int compare(File f1, File f2) {
							return Long.valueOf(f1.lastModified()).compareTo(
									f2.lastModified());
						}
					});

					// Remove Files
					for (int i = 0; i < files.size(); i++) {
						freeSpace = ((double) folder.getUsableSpace() / (double) folder
								.getTotalSpace()) * 100.0;
						if (freeSpace < minPercent) {
							if (files.get(i).exists()) {
								System.out.println("Removing "
										+ files.get(i).getAbsolutePath());
								if (files.get(i).isDirectory())
									try {
										FileUtils.deleteDirectory(files.get(i));
									} catch (IOException e) {
										System.err
												.println("Unabled to remove directory "
														+ files.get(i)
																.getAbsolutePath());
									}
								else
									files.get(i).delete();
							}
						}
					}

				}
			}
		};
	}

	/***************************************************************************
	 * Getters and Setters *
	 **************************************************************************/

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public int getMinMinutes() {
		return minMinutes;
	}

	public void setMinMinutes(int minMinutes) {
		this.minMinutes = minMinutes;
	}

	public double getMinPercent() {
		return minPercent;
	}

	public void setMinPercent(double minPercent) {
		this.minPercent = minPercent;
	}

	public boolean isRecursive() {
		return recursive;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public boolean isRemoveDir() {
		return removeDir;
	}

	public void setRemoveDir(boolean removeDir) {
		this.removeDir = removeDir;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	public TimerTask getTask() {
		return task;
	}

	public void setTask(TimerTask task) {
		this.task = task;
	}

	/***************************************************************************
	 * Methods *
	 **************************************************************************/

}
