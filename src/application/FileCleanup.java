package application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;

/***************************************************************************
 * FileDropper *
 **************************************************************************/

public class FileCleanup extends JPanel {

	/***************************************************************************
	 * Class Variables *
	 **************************************************************************/

	private static final long serialVersionUID = -4850915807992831747L;
	private int filterNum;
	private TablePanel listPanel;
	private FilterPanel listFilterPanel;
	private JSplitPane horizontalSplitPane;
	private LinkedList<FILTER> filterList = new LinkedList<FILTER>();
	private Properties properties = null;
	private String config;

	/***************************************************************************
	 * Constructors
	 **************************************************************************/

	public FileCleanup(String config) {

		// Load Configuration File
		loadConfigFile(config);

		// Create and Show GUI
		createAndShowGUI();
	}

	/***************************************************************************
	 * Methods *
	 **************************************************************************/

	public void createAndShowGUI() {

		// Create Table Panel
		listPanel = new TablePanel();
		listPanel.setTableListener(new TableListener() {

			public void tableChanged(List<FILTER> filterList) {
				saveConfigFile(filterList);
			}
		});

		// Create Filter Panel
		listFilterPanel = new FilterPanel();
		listFilterPanel.setFilterListener(new FilterListener() {

			public void addFilter(FILTER filter) {
				listPanel.addRow(filter);
				saveConfigFile(listPanel.getTableModel().getFilterList());
			}
		});

		// Load Configuration File
		for (int i = 0; i < filterList.size(); i++) {
			listPanel.addRow(filterList.get(i));
		}

		horizontalSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				listPanel, listFilterPanel);
		horizontalSplitPane.setResizeWeight(1);
		horizontalSplitPane.setOneTouchExpandable(true);

		// Layout GUI
		layoutGUI();
	}

	public void layoutGUI() {

		// Set Layout
		setLayout(new BorderLayout());

		// Add Components
		add(horizontalSplitPane, BorderLayout.CENTER);
	}

	public void loadConfigFile(String config) {

		// Load Configuration File
		this.config = config;
		if ((config != null) && (!config.isEmpty())) {
			File configFile = new File(config);
			if (!configFile.exists())
				try {
					configFile.createNewFile();
				} catch (IOException e) {
					System.err.println("Unable to create file "
							+ configFile.getAbsolutePath());
				}

			properties = new Properties();
			try {
				InputStream inputStream = new FileInputStream(configFile);
				properties.load(inputStream);
				inputStream.close();
			} catch (IOException e) {
				System.err.println("Unable to load config file "
						+ configFile.getAbsolutePath());
			}

			filterNum = Integer.parseInt(properties.getProperty("NUM", "0"));
			for (int i = 0; i < filterNum; i++) {
				FILTER filter = new FILTER();
				filter.setPath(properties.getProperty("PATH" + i));
				filter.setPrefix(properties.getProperty("PREFIX" + i));
				filter.setSuffix(properties.getProperty("SUFFIX" + i));
				filter.setMinMinutes(Integer.parseInt(properties
						.getProperty("MINMIN" + i)));
				filter.setMinPercent(Double.parseDouble(properties
						.getProperty("MINPERC" + i)));
				filter.setRecursive(Boolean.parseBoolean(properties
						.getProperty("RECURSIVE" + i)));
				filter.setRemoveDir(Boolean.parseBoolean(properties
						.getProperty("DIRECTORIES" + i)));
				filterList.add(filter);
			}
		}
	}

	public void saveConfigFile(List<FILTER> filterList) {
		System.out.println("SAVING");
		if (properties != null) {
			properties.clear();
			properties.setProperty("NUM", Integer.toString(filterList.size()));
			for (int i = 0; i < filterList.size(); i++) {
				properties.setProperty("PATH" + i, filterList.get(i).getPath());
				properties.setProperty("PREFIX" + i, filterList.get(i)
						.getPrefix());
				properties.setProperty("SUFFIX" + i, filterList.get(i)
						.getSuffix());
				properties.setProperty("MINMIN" + i,
						Integer.toString(filterList.get(i).getMinMinutes()));
				properties.setProperty("MINPERC" + i,
						Double.toString(filterList.get(i).getMinPercent()));
				properties.setProperty("RECURSIVE" + i,
						Boolean.toString(filterList.get(i).isRecursive()));
				properties.setProperty("DIRECTORIES" + i,
						Boolean.toString(filterList.get(i).isRemoveDir()));
			}
			try {
				FileOutputStream outputFile = new FileOutputStream(config);
				properties.store(outputFile, null);
				outputFile.close();
			} catch (Exception e) {
				System.out.println("Unable to save config file " + config);
			}
		}
	}

	/***************************************************************************
	 * Main *
	 **************************************************************************/

	public static void main(String[] args) {

		StringBuilder builder = new StringBuilder();

		if (args.length > 0) {
			builder.append(args[0]);
		} else {
			System.out.println("No Configuration File");
		}
		final String config = builder.toString();

		// Swing Runnable
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Setup UI Manager
				try {
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Setup Frame
				JPopupMenu.setDefaultLightWeightPopupEnabled(false);
				ToolTipManager.sharedInstance().setLightWeightPopupEnabled(
						false);
				JFrame frame = new JFrame();
				FileCleanup fileCleanup = new FileCleanup(config);
				frame.add(fileCleanup);

				// Set Frame Properties
				frame.setMinimumSize(new Dimension(1200, 600));
				frame.setSize(1200, 600);
				frame.setLocationRelativeTo(null);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}
}
