package utils;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CleanupUtils {

	public static Collection<File> listFileTree(File dir, boolean recursive,
			boolean directories) {
		Set<File> fileTree = new HashSet<File>();
		try {
			for (File entry : dir.listFiles()) {
				if (entry.isFile()) {
					fileTree.add(entry);
				} else {
					if (directories)
						fileTree.add(entry);
					if (recursive)
						fileTree.addAll(listFileTree(entry, recursive,
								directories));
				}
			}
		} catch (Exception e) {

		}
		return fileTree;
	}

}
